import * as THREE from 'three';
import { World } from '../world/World';
import { IUpdatable } from '../interfaces/IUpdatable';
export declare class LuckyTrigger extends THREE.Object3D implements IUpdatable {
    updateOrder: number;
    world: World;
    isInner: boolean;
    constructor(gltf: THREE.Object3D, world: World);
    update(timestep: number, unscaledTimeStep: number): void;
    enterHandler(): void;
}
