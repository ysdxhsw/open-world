# 📒OPEN-WORLD

## 📖简介
基于three.js和cannon.js以及建模知识的开放世界基础框架, 用于帮助开发者快速搭建个人的开放世界。

## 安装
使用较低版本node进行安装和启动，否则可能会出现问题。已测node18是有问题，node16是没有问题的。

## 体验地址
地址:https://gujyang.gitee.io/open-world
没钱搞服务器，就用了git的地址，所以有时候会比较卡，尤其是第一次加载glb资源的时候,可能需要点开挂机3，4分钟，如果嫌弃慢可以自己拉取代码进行启动

## 源码介绍文档
https://gujyang.gitee.io/myblog/2024/01/07/after-three-4
## 页面截图
<img src='https://gujyang.gitee.io/image-save-5/after-three/4/effect63.png' />

## 大佬源码地址
本代码基础框架由github的一个大佬开源，在此基础上我进行自己的整改
地址:https://github.com/swift502/Sketchbook

## 求赞
如果觉得这个框架对你有帮助，欢迎点个star，谢谢。

## blend
如果大家需要我的建模模型文件，可以来私聊我。

